FROM arm32v7/python:3.7-bullseye

RUN apt-get update && apt-get install -y g++ make

COPY . /app

WORKDIR /app

RUN pip install pyserial paho-mqtt==1.6.1 requests pyyaml

CMD ["python", "ControllarServer.py"]