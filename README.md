This project is the low-level webserver that runs on the Controllar home automation installation.
It is the bridge between the serial port and the docker that communicates with the external server.

It is meant to use with one of the Controllar's base images.

You also need a `.db3` file provided by Controllar which describes your home automation and a `.ovpn` to connect to the system.

This software should already be installed in one of the Controllar's base images.

To configure, copy the `.db3` and `.ovpn` files to `/controllar` folder, than, by ssh:

```
cd /controllar
./maleta.sh # select option 1, 3, 1
reboot
```

### Configuring the IR/RF commands

The `maleta.sh` script already configures the following multimedia options on config.ini:

```
mqtt_broker_port= 1883"
irconfig_file=<your-instalation-name>.yaml
broadlink_cmd_folder=/controllar/broadlink-mqtt/commands
encodeir=/controllar/MakeHex/encodeir
```

* `mqtt_broaker` and `mqtt_broaker_port` are the location for the mqtt broker
  such as mosquitto.
* `irconfig_file` is the yaml that configures the ir/rf signals (see below).
* `broadlink_cmd_folder` points to where the commands are and will be saved,
  usually inside broadlink-mqtt instalation folder. The signals are converted to
  the broadlink format.
* `encodeir` is the binary that convert a protocol format to raw.

To configure Luminos to emit IR/RF commands, configure an yaml file on `/controllar` folder.

Example:

```
    1:
      emitter: broadlink
      command: tv/samsung/volumeup
    7:
      emitter: broadlink
      command: tv/samsung/volumedown
    28:
      signal: rf
      emitter: broadlink
      command: rfkit/bt1
    29:
      emitter: thmedia
      command: tv/samsung/mute
      channel: 2
    3:
      emitter: broadlink
      command:
        protocol: NECx2
        device: 7
        subdevice: 7
        function: 15
```

Signals are saved under `broadlink_cmd_folder/`

default values are:
    signal: ir
    channel: 1
    freq: 38000

The frequency is defined on broadlink format header and current ignored by thmedia.
