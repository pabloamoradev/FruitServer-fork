from RFIRLumino import RFIRLuminoThmedia
from RFIRLumino import RFIRLuminoBroadlink
from RFIRLumino import RFIRLuminoList
import unittest
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import mock_open


class TestRFIRLumino(unittest.TestCase):

    # broadlink samsung tv volumeup
    cmd1_broadlink = "26008c009395103a10391039111410151015101411141039113910391114101510151014101510391139101510391015101411141015101510141139101411391039103a10391000060092951139103911391014111410151015101411391039103a10141114101510151014103a103910151039101510151014111410151015103910151039103a1039103a10000d05000000000000000000000000"  # noqa
    pul1_array = [4476, 4537, 487, 1766, 487, 1735, 487, 1735, 517, 609, 487, 639, 487, 639, 487, 609, 517, 609, 487, 1735, 517, 1735, 487, 1735, 517, 609, 487, 639, 487, 639, 487, 609, 487, 639, 487, 1735, 517, 1735, 487, 639, 487, 1735, 487, 639, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 517, 1735, 487, 609, 517, 1735, 487, 1735, 487, 1766, 487, 1735, 487, 46776, 4446, 4537, 517, 1735, 487, 1735, 517, 1735, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 517, 1735, 487, 1735, 487, 1766, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 487, 1766, 487, 1735, 487, 639, 487, 1735, 487, 639, 487, 639, 487, 609, 517, 609, 487, 639, 487, 639, 487, 1735, 487, 639, 487, 1735, 487, 1766, 487, 1735, 487, 1766, 487]  # noqa
    pul1_raw = "4476,4537,487,1766,487,1735,487,1735,517,609,487,639,487,639,487,609,517,609,487,1735,517,1735,487,1735,517,609,487,639,487,639,487,609,487,639,487,1735,517,1735,487,639,487,1735,487,639,487,609,517,609,487,639,487,639,487,609,517,1735,487,609,517,1735,487,1735,487,1766,487,1735,487,46776,4446,4537,517,1735,487,1735,517,1735,487,609,517,609,487,639,487,639,487,609,517,1735,487,1735,487,1766,487,609,517,609,487,639,487,639,487,609,487,1766,487,1735,487,639,487,1735,487,639,487,639,487,609,517,609,487,639,487,639,487,1735,487,639,487,1735,487,1766,487,1735,487,1766,487"  # noqa
    # broadlink samsung tv power
    cmd2_broadlink = "26008c0092961039103a1039101510151014101510151039103a10391015101411141015101510141139101510141114101510151014103a10141139103911391037123a10391000060092961039103911391014111410151015101411391039103a101411141015101510141015103911141015101510141015101510391015103911391039103a1039103911000d05000000000000000000000000"  # noqa
    pul2_array = [4446, 4568, 487, 1735, 487, 1766, 487, 1735, 487, 639, 487, 639, 487, 609, 487, 639, 487, 639, 487, 1735, 487, 1766, 487, 1735, 487, 639, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 517, 1735, 487, 639, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 487, 1766, 487, 609, 517, 1735, 487, 1735, 517, 1735, 487, 1674, 548, 1766, 487, 1735, 487, 46776, 4446, 4568, 487, 1735, 487, 1735, 517, 1735, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 517, 1735, 487, 1735, 487, 1766, 487, 609, 517, 609, 487, 639, 487, 639, 487, 609, 487, 639, 487, 1735, 517, 609, 487, 639, 487, 639, 487, 609, 487, 639, 487, 639, 487, 1735, 487, 639, 487, 1735, 517, 1735, 487, 1735, 487, 1766, 487, 1735, 487, 1735, 517]  # noqa
    pul2_raw = "4446,4568,487,1735,487,1766,487,1735,487,639,487,639,487,609,487,639,487,639,487,1735,487,1766,487,1735,487,639,487,609,517,609,487,639,487,639,487,609,517,1735,487,639,487,609,517,609,487,639,487,639,487,609,487,1766,487,609,517,1735,487,1735,517,1735,487,1674,548,1766,487,1735,487,46776,4446,4568,487,1735,487,1735,517,1735,487,609,517,609,487,639,487,639,487,609,517,1735,487,1735,487,1766,487,609,517,609,487,639,487,639,487,609,487,639,487,1735,517,609,487,639,487,639,487,609,487,639,487,639,487,1735,487,639,487,1735,517,1735,487,1735,487,1766,487,1735,487,1735,517"  # noqa

    # samsung tv mute
    prot1 = {"protocol": "NECx2", "device": 7, "subdevice": 7, "function": 15}
    prot1_encode = "4512 4512 564 1692 564 1692 564 1692 564 564 564 564 564 564 564 564 564 564 564 1692 564 1692 564 1692 564 564 564 564 564 564 564 564 564 564 564 1692 564 1692 564 1692 564 1692 564 564 564 564 564 564 564 564 564 564 564 564 564 564 564 564 564 1692 564 1692 564 1692 564 1692 564 43992".encode('utf-8')  # noqa
    prot1_res = "4512,4512,564,1692,564,1692,564,1692,564,564,564,564,564,564,564,564,564,564,564,1692,564,1692,564,1692,564,564,564,564,564,564,564,564,564,564,564,1692,564,1692,564,1692,564,1692,564,564,564,564,564,564,564,564,564,564,564,564,564,564,564,564,564,1692,564,1692,564,1692,564,1692,564,43992"  # noqa
    prot1_broadlink = "26004900949413381338133813131313131313131313133813381338131313131313131313131338133813381338131313131313131313131313131313131338133813381338130005a5000d05000000000000000000000000"  # noqa

    prot2 = {"protocol": "NECx1", "device": 16, "subdevice": 16,
             "function": 248}
    prot2_encode = "4512 4512 564 564 564 564 564 564 564 564 564 1692 564 564 564 564 564 564 564 564 564 564 564 564 564 564 564 1692 564 564 564 564 564 564 564 564 564 564 564 564 564 1692 564 1692 564 1692 564 1692 564 1692 564 1692 564 1692 564 1692 564 564 564 564 564 564 564 564 564 564 564 43992 4512 4512 564 1692 564 95880".encode('utf-8')  # noqa
    prot2_res = "4512,4512,564,564,564,564,564,564,564,564,564,1692,564,564,564,564,564,564,564,564,564,564,564,564,564,564,564,1692,564,564,564,564,564,564,564,564,564,564,564,564,564,1692,564,1692,564,1692,564,1692,564,1692,564,1692,564,1692,564,1692,564,564,564,564,564,564,564,564,564,564,564,43992,4512,4512,564,1692,564,95880"  # noqa
    prot2_broadlink = "26005100949413131313131313131338131313131313131313131313131313381313131313131313131313131338133813381338133813381338133813131313131313131313130005a59494133813000c4c000d05000000000000000000000000"  # noqa

    # samsung tv power off
    sendir_command = "sendir,1:1,1,38000,1,1,172,172,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,21,21,21,21,64,21,64,21,64,21,64,21,21,21,21,21,64,21,64,21,21,21,1673" # noqa
    sendir_res = "4526,4526,552,1684,552,1684,552,1684,552,552,552,552,552,552,552,552,552,552,552,1684,552,1684,552,1684,552,552,552,552,552,552,552,552,552,552,552,552,552,552,552,552,552,1684,552,1684,552,552,552,552,552,1684,552,1684,552,1684,552,1684,552,552,552,552,552,1684,552,1684,552,552,552,44026"  # noqa
    sendir_command_broadlink = "26004900959512371237123712121212121212121212123712371237121212121212121212121212121212121237123712121212123712371237123712121212123712371212120005a6000d05000000000000000000000000" # noqa
    sendir_command_hash = "c13d6116debc6ee04bf8826e78b6c7139a95a6e50a0cddc2db2670f931464aae"  # noqa

    intensity = 50

    # samsung tv volumeup recorded on thmedia
    ir_signal_rec = "4528,4478,580,1658,572,1666,574,1664,576,542,578,542,578,540,580,540,580,538,582,1656,572,1664,576,1662,578,540,580,538,582,538,572,546,572,546,574,1664,576,1662,578,1658,582,538,582,536,572,546,574,546,574,544,576,542,578,542,578,540,580,1658,572,1666,574,1664,576,1660,580,1658,572,0xE0E0E01F".encode('utf-8')  # noqa
    ir_signal_raw = "4528,4478,580,1658,572,1666,574,1664,576,542,578,542,578,540,580,540,580,538,582,1656,572,1664,576,1662,578,540,580,538,582,538,572,546,572,546,574,1664,576,1662,578,1658,582,538,582,536,572,546,574,546,574,544,576,542,578,542,578,540,580,1658,572,1666,574,1664,576,1660,580,1658,572"  # noqa

    rf_signal_rec = "11437,477,548,986,546,992,1060,477,1056,480,1058,478,1056,480,1057,479,1058,480,538,1003,1049,486,539,999,1054,479,542,995,1052,483,543,994,1053,486,540,994,1053,484,542,993,1055,484,1052,482,1057,478,1055,485,538,998,544,991,1056,479,544,995,1052,".encode('utf-8')  # noqa
    rf_signal_raw = "11437,477,548,986,546,992,1060,477,1056,480,1058,478,1056,480,1057,479,1058,480,538,1003,1049,486,539,999,1054,479,542,995,1052,483,543,994,1053,486,540,994,1053,484,542,993,1055,484,1052,482,1057,478,1055,485,538,998,544,991,1056,479,544,995,1052,11437,477,548,986,546,992,1060,477,1056,480,1058,478,1056,480,1057,479,1058,480,538,1003,1049,486,539,999,1054,479,542,995,1052,483,543,994,1053,486,540,994,1053,484,542,993,1055,484,1052,482,1057,478,1055,485,538,998,544,991,1056,479,544,995,1052"  # noqa

    def test__is_broadlink_file(self):
        m_mqtt_client = MagicMock()
        o = RFIRLuminoThmedia(m_mqtt_client, None, None)
        self.assertTrue(o._is_broadlink_file("this/is/file"))
        self.assertFalse(o._is_broadlink_file(1))
        self.assertFalse(o._is_broadlink_file({}))
        self.assertFalse(o._is_broadlink_file([]))

    def test__is_protocol(self):
        m_mqtt_client = MagicMock()
        o = RFIRLuminoThmedia(m_mqtt_client, None, None)
        self.assertFalse(o._is_protocol("this/is/file"))
        self.assertFalse(o._is_protocol({}))
        self.assertFalse(o._is_protocol([]))
        self.assertFalse(o._is_protocol({"protocol": 1}))
        self.assertFalse(o._is_protocol({"funcion": 2}))
        self.assertFalse(o._is_protocol({"device": 3}))
        self.assertFalse(o._is_protocol({"subdevice": 4}))
        self.assertTrue(o._is_protocol({"protocol": 34,
                                        "function": 23,
                                        "device": 123,
                                        "subdevice": "foo"}))

    def test__is_sendir_command(self):
        m_mqtt_client = MagicMock()
        o = RFIRLuminoThmedia(m_mqtt_client, None, None)
        self.assertFalse(o._is_sendir_command("this/is/file"))
        self.assertTrue(o._is_sendir_command("sendir,1:1,1,38000,1,1,172,172,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,64"))  # noqa
        self.assertFalse(o._is_sendir_command(str({"protocol": 34,
                                                   "function": 23,
                                                   "device": 123,
                                                   "subdevice": "foo"})))


class TestRFIRLuminoThmedia(TestRFIRLumino):

    def test__broadlink_to_pulesarray(self):
        m_mqtt_client = MagicMock()
        o = RFIRLuminoThmedia(m_mqtt_client, None, None)
        self.assertEqual(o._broadlink_to_pulesarray(self.cmd1_broadlink),
                         self.pul1_array)
        self.assertEqual(o._broadlink_to_pulesarray(self.cmd2_broadlink),
                         self.pul2_array)

    # @patch('builtins.open', mock_open(read_data='test'))
    def test__broadlink_file_to_raw(self):
        with patch('builtins.open',
                   mock_open(read_data=self.cmd1_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "/test/folder", None,
                                  mac="ff_ff_ff_ff_ff")
            self.assertEqual(o._broadlink_file_to_raw("file"),
                             "30,38000," + self.pul1_raw)
            m.assert_called_once_with('/test/folder/file')

        with patch('builtins.open',
                   mock_open(read_data=self.cmd1_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "/test/foo/", None,
                                  mac="ff_ff_ff_ff_ff", freq=32)
            self.assertEqual(o._broadlink_file_to_raw("file"),
                             "30,32," + self.pul1_raw)
            m.assert_called_once_with('/test/foo/file')

        with patch('builtins.open',
                   mock_open(read_data=self.cmd2_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "/test/folder", None)
            self.assertEqual(o._broadlink_file_to_raw("file"),
                             "30,38000," + self.pul2_raw)
            m.assert_called_once_with('/test/folder/file')

        with patch('builtins.open',
                   mock_open(read_data=self.cmd2_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "/test/foo/", None,
                                  mac="ff_ff_ff_ff_ff", freq=32)
            self.assertEqual(o._broadlink_file_to_raw("file"),
                             "30,32," + self.pul2_raw)
            m.assert_called_once_with('/test/foo/file')

    def test__protocol_to_raw(self):
        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot1_encode)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, None, "encodeir")
            self.assertEqual(o._protocol_to_raw(self.prot1["protocol"],
                                                self.prot1["device"],
                                                self.prot1["subdevice"],
                                                self.prot1["function"]),
                             "30,38000," + self.prot1_res)
            m.assert_called_once_with(["encodeir",
                                       self.prot1["protocol"],
                                       str(self.prot1["device"]),
                                       str(self.prot1["subdevice"]),
                                       str(self.prot1["function"])
                                       ])

        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot2_encode)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, None, "encodeir", freq=42)
            self.assertEqual(o._protocol_to_raw(self.prot2["protocol"],
                                                self.prot2["device"],
                                                self.prot2["subdevice"],
                                                self.prot2["function"]),
                             "30,42," + self.prot2_res)
            m.assert_called_once_with(["encodeir",
                                       self.prot2["protocol"],
                                       str(self.prot2["device"]),
                                       str(self.prot2["subdevice"]),
                                       str(self.prot2["function"])
                                       ])

    def test__get_raw_command(self):
        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot1_encode)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, None, "my_prog")
            self.assertEqual(o._get_raw_command(self.prot1),
                             "30,38000," + self.prot1_res)
            m.assert_called_once_with(["my_prog",
                                       self.prot1["protocol"],
                                       str(self.prot1["device"]),
                                       str(self.prot1["subdevice"]),
                                       str(self.prot1["function"])
                                       ])

        m_mqtt_client = MagicMock()
        o = RFIRLuminoThmedia(m_mqtt_client, None, {})
        self.assertRaises(Exception, o._get_raw_command)

        o = RFIRLuminoThmedia(m_mqtt_client, None)
        self.assertEqual(o._get_raw_command(self.sendir_command),
                         "30,38000," + self.sendir_res)

        with patch('builtins.open',
                   mock_open(read_data=self.cmd1_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "/foo/bar/", None)
            self.assertEqual(o._get_raw_command("my_cmd"),
                             "30,38000," + self.pul1_raw)
            m.assert_called_once_with('/foo/bar/my_cmd')

    def test_activate(self):
        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot1_encode)) as m:
            mock_logging = MagicMock()
            mock_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(mock_mqtt_client, None,
                                  "my_prog", channel=1,
                                  logging=mock_logging)
            o.command_to_mqtt(None, self.prot1)
            o.activate(self.intensity)
            mock_mqtt_client.publish.assert_called_once_with(
                "ir_server/FF:FF:FF:FF:FF:FF/send_1", "30,38000,"
                + self.prot1_res)
            m.assert_called_once_with(["my_prog",
                                       self.prot1["protocol"],
                                       str(self.prot1["device"]),
                                       str(self.prot1["subdevice"]),
                                       str(self.prot1["function"])
                                       ])
            mock_logging.debug.assert_called_once()

        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot2_encode)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo", "my_prog",
                                  freq=123, channel=55)
            o.command_to_mqtt(None, self.prot2)
            o.activate(self.intensity)
            m_mqtt_client.publish.assert_called_once_with(
                "ir_server/FF:FF:FF:FF:FF:FF/send_55", "30,123,"
                + self.prot2_res)
            m.assert_called_once_with(["my_prog",
                                       self.prot2["protocol"],
                                       str(self.prot2["device"]),
                                       str(self.prot2["subdevice"]),
                                       str(self.prot2["function"])
                                       ])

        with patch('builtins.open',
                   mock_open(read_data=self.cmd1_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo/bar/", channel=1)
            o.command_to_mqtt(None, "my_cmd")
            o.activate(self.intensity)
            m_mqtt_client.publish.assert_called_once_with(
                "ir_server/FF:FF:FF:FF:FF:FF/send_1", "30,38000,"
                + self.pul1_raw)
            m.assert_called_once_with('foo/bar/my_cmd')

        with patch('builtins.open',
                   mock_open(read_data=self.cmd2_broadlink)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo", "my_prog",
                                  freq=123, channel=55)
            o.command_to_mqtt(None, "my_cmd")
            o.activate(self.intensity)
            m_mqtt_client.publish.assert_called_once_with(
                "ir_server/FF:FF:FF:FF:FF:FF/send_55", "30,123,"
                + self.pul2_raw)
            m.assert_called_once_with('foo/my_cmd')

        # command alreay recorded
        with patch('os.path.exists',
                   MagicMock(return_value=True)) as m:
            with patch('builtins.open',
                       mock_open(read_data=("30,1,"+self.ir_signal_raw))) as m:
                m_mqtt_client = MagicMock()
                o = RFIRLuminoThmedia(m_mqtt_client, "foo",
                                      freq=1, channel=55)
                o.command_to_mqtt(None, "my_cmd")
                o.activate(self.intensity)
                m_mqtt_client.publish.assert_called_once_with(
                    "ir_server/FF:FF:FF:FF:FF:FF/send_55", "30,1,"
                    + self.ir_signal_raw)
                m.assert_called_with('foo/my_cmd')

        # recording failed
        with patch('os.path.exists',
                   MagicMock(return_value=False)) as m:
            m_mqtt_client = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo",
                                  freq=123, channel=55)
            o.record_time = 1
            o.msg_recorded = self.ir_signal_raw
            o.command_to_mqtt(None, "my_cmd")
            o.activate(self.intensity)
            m_mqtt_client.publish.assert_not_called()
            m.assert_called_with('foo/my_cmd')

    def test__ir_record_signal(self):
        m_mqtt_client = MagicMock()
        mock_logging = MagicMock()
        o = RFIRLuminoThmedia(m_mqtt_client, None, logging=mock_logging)
        o.record_time = 1
        o.msg_recorded = self.ir_signal_raw
        self.assertTrue(o._ir_record_signal())
        o.msg_recorded = ""
        self.assertFalse(o._ir_record_signal())

    def test__rf_record_signal(self):
        m_mqtt_client = MagicMock()
        mock_logging = MagicMock()
        o = RFIRLuminoThmedia(m_mqtt_client, None, signal="rf",
                              logging=mock_logging)
        o.record_time = 1
        o.msg_recorded = self.rf_signal_raw
        self.assertTrue(o._rf_record_signal())
        o.msg_recorded = ""
        self.assertFalse(o._rf_record_signal())

    def test_on_message(self):
        with patch('builtins.open',
                   mock_open(read_data=self.ir_signal_rec)) as m:
            m_mqtt_client = MagicMock()
            mock_logging = MagicMock()
            mock_message = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo/bar/", None,
                                  logging=mock_logging)
            o.command_file = "my_cmd"
            o.on_message(None, None, mock_message)
            m_mqtt_client.unsubscribe.assert_called_once_with(
              "ir_server/FF:FF:FF:FF:FF:FF/received")
            m.assert_called_once_with('foo/bar/my_cmd', 'w')

        with patch('builtins.open',
                   mock_open(read_data=self.rf_signal_rec)) as m:
            m_mqtt_client = MagicMock()
            mock_logging = MagicMock()
            mock_message = MagicMock()
            o = RFIRLuminoThmedia(m_mqtt_client, "foo/bar/", None, signal="rf",
                                  logging=mock_logging)
            o.command_file = "my_cmd"
            o.on_message(None, None, mock_message)
            m_mqtt_client.unsubscribe.assert_called_once_with(
              "ir_server/FF:FF:FF:FF:FF:FF/received_rf")
            m.assert_called_once_with('foo/bar/my_cmd', 'w')


class TestRFIRLuminoBroadlink(TestRFIRLumino):

    def test__pulesarray_to_broadlink(self):
        o = RFIRLuminoBroadlink(None, None, None, None)
        self.assertEqual(o._pulesarray_to_broadlink(self.pul1_array, 38000),
                         self.cmd1_broadlink)
        self.assertEqual(o._pulesarray_to_broadlink(self.pul2_array, 38000),
                         self.cmd2_broadlink)

    def test__protocol_to_broadlink(self):
        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot1_encode)) as m:
            o = RFIRLuminoBroadlink(None, None)
            self.assertEqual(o._protocol_to_broadlink(self.prot1["protocol"],
                                                      self.prot1["device"],
                                                      self.prot1["subdevice"],
                                                      self.prot1["function"]),
                             self.prot1_broadlink)
            m.assert_called_once_with(["encodeir",
                                       self.prot1["protocol"],
                                       str(self.prot1["device"]),
                                       str(self.prot1["subdevice"]),
                                       str(self.prot1["function"])
                                       ])

        with patch('subprocess.check_output',
                   MagicMock(return_value=self.prot2_encode)) as m:
            o = RFIRLuminoBroadlink(None, None, "my_prog")
            self.assertEqual(o._protocol_to_broadlink(self.prot2["protocol"],
                                                      self.prot2["device"],
                                                      self.prot2["subdevice"],
                                                      self.prot2["function"]),
                             self.prot2_broadlink)
            m.assert_called_once_with(["my_prog",
                                       self.prot2["protocol"],
                                       str(self.prot2["device"]),
                                       str(self.prot2["subdevice"]),
                                       str(self.prot2["function"])
                                       ])

    def test__get_broadlink_command(self):
        o = RFIRLuminoBroadlink(None, None)
        self.assertEqual(o._get_broadlink_command("tv/bar/foo"),
                         "broadlink/ff_ff_ff_ff_ff_ff/tv/bar/foo")

        o = RFIRLuminoBroadlink(None, None, signal="rf")
        self.assertRaises(Exception, o._get_broadlink_command, self.prot1)

        o = RFIRLuminoBroadlink(None, None, signal="rf")
        self.assertEqual(o._get_broadlink_command("tv/bar/foo"),
                         "broadlink/ff_ff_ff_ff_ff_ff/tv/bar/foo")

        o = RFIRLuminoBroadlink(None, None)
        self.assertRaises(Exception, o._get_broadlink_command, {})

        o = RFIRLuminoBroadlink(None, None)
        self.assertRaises(Exception, o._get_broadlink_command, [])

        with patch('os.path.exists',
                   MagicMock(return_value=True)) as m:
            o = RFIRLuminoBroadlink(None, "/foo/bar")
            path = (self.prot1["protocol"] + "/" +
                    str(self.prot1["device"]) + "/" +
                    str(self.prot1["subdevice"]) + "/" +
                    str(self.prot1["function"]))
            self.assertEqual(o._get_broadlink_command(self.prot1),
                             "broadlink/ff_ff_ff_ff_ff_ff/" + path)
            m.assert_called_once_with('/foo/bar/' + path)

        with patch('os.path.exists',
                   MagicMock(return_value=True)) as m:
            o = RFIRLuminoBroadlink(None, "/foo/bar")
            self.assertEqual(o._get_broadlink_command(self.sendir_command),
                             "broadlink/ff_ff_ff_ff_ff_ff/" + "sendir-" +
                             self.sendir_command_hash)
            m.assert_called_once_with('/foo/bar/' + "sendir-" +
                                      self.sendir_command_hash)

        with patch('os.path.exists',
                   MagicMock(return_value=False)) as m:
            with patch('subprocess.check_output',
                       MagicMock(side_effect=Exception(
                        'returned non-zero exit status 255'))):
                o = RFIRLuminoBroadlink(None, "/foo/bar")
                self.assertRaises(Exception, o._get_broadlink_command,
                                  self.prot1)

    @patch('os.makedirs')
    @patch('os.path.exists')
    @patch('subprocess.check_output')
    def test__get_broadlink_command_prot(self, m_enc, m_exist, m_mkdir):
        m_enc.return_value = self.prot1_encode
        m_exist.return_value = False

        with patch('builtins.open', mock_open()) as m_open:
            o = RFIRLuminoBroadlink(None, "foo/bar")
            path = (self.prot1["protocol"] + "/" +
                    str(self.prot1["device"]) + "/" +
                    str(self.prot1["subdevice"]) + "/" +
                    str(self.prot1["function"]))
            self.assertEqual(o._get_broadlink_command(self.prot1),
                             "broadlink/ff_ff_ff_ff_ff_ff/" + path)
            m_enc.assert_called_once_with(["encodeir",
                                          self.prot1["protocol"],
                                          str(self.prot1["device"]),
                                          str(self.prot1["subdevice"]),
                                          str(self.prot1["function"])])
            full_path = 'foo/bar/' + path
            m_exist.assert_called_once_with(full_path)
            m_open.assert_called_once_with(full_path, 'w')
            m_mkdir.assert_called_once()
            handle = m_open()
            handle.write.assert_called_once_with(self.prot1_broadlink)

    @patch('os.makedirs')
    @patch('os.path.exists')
    def test__get_broadlink_command_sendir(self, m_exist, m_mkdir):
        m_exist.return_value = False

        with patch('builtins.open', mock_open()) as m_open:
            o = RFIRLuminoBroadlink(None, "foo/bar")
            self.assertEqual(o._get_broadlink_command(self.sendir_command),
                             "broadlink/ff_ff_ff_ff_ff_ff/" + "sendir-" +
                             self.sendir_command_hash)
            full_path = 'foo/bar/' + "sendir-" + self.sendir_command_hash
            m_exist.assert_called_once_with(full_path)
            m_open.assert_called_once_with(full_path, 'w')
            m_mkdir.assert_called_once()
            handle = m_open()
            handle.write.assert_called_once_with(self.sendir_command_broadlink)

    def test_activate(self):
        mock_logging = MagicMock()
        mock_mqtt_client = MagicMock()
        o = RFIRLuminoBroadlink(mock_mqtt_client, None,
                                logging=mock_logging)
        o.command_to_mqtt({self.intensity: "foo/bar"}, None)
        o.activate(self.intensity)
        mock_mqtt_client.publish.assert_called_once_with(
          "broadlink/ff_ff_ff_ff_ff_ff/foo/bar", "auto")

        mock_mqtt_client = MagicMock()
        o = RFIRLuminoBroadlink(mock_mqtt_client, None, signal="rf")
        o.command_to_mqtt(None, "foo/bar")
        o.activate(self.intensity)
        mock_mqtt_client.publish.assert_called_once_with(
          "broadlink/ff_ff_ff_ff_ff_ff/foo/bar", "autorf")
        mock_logging.debug.assert_called_once()

        mock_mqtt_client = MagicMock()
        o = RFIRLuminoBroadlink(mock_mqtt_client, None,
                                mac="aa_bb_cc_dd_ee_ff",
                                logging=mock_logging)
        o.command_to_mqtt({self.intensity: "foo/bar"}, None)
        o.activate(self.intensity)
        mock_mqtt_client.publish.assert_called_once_with(
          "broadlink/aa_bb_cc_dd_ee_ff/foo/bar", "auto")

        o = RFIRLuminoBroadlink(mock_mqtt_client, None,
                                logging=mock_logging)
        o.command_to_mqtt("foo/bar", None)
        o.activate(self.intensity)
        mock_logging.error.assert_called_with(
          'Could not activate lumino')


class TestIRRFLuminoList(unittest.TestCase):

    intensity = 50
    config = {
        1: {'command': 'tv/samsung/volumeup', 'emitter': 'broadlink'},
        2: {'command': 'tv/samsung/channelup', 'emitter': 'broadlink'},
        4: {'command': 'tv/samsung/up', 'emitter': 'broadlink'},
        7: {'command': 'tv/samsung/volumedown', 'emitter': 'broadlink'},
        8: {'command': 'tv/samsung/channeldown', 'emitter': 'broadlink'},
        9: {'command': 'tv/samsung/left', 'emitter': 'broadlink'},
        10: {'command': 'tv/samsung/down', 'emitter': 'broadlink'},
        11: {'command': 'tv/samsung/right', 'emitter': 'broadlink'},
        14: {'command': 'tv/samsung/mute', 'emitter': 'broadlink'},
        25: {'command': 'tv/samsung/power', 'emitter': 'broadlink'},
        26: {'channel': 1, 'command': 'tv/samsung/power',
             'emitter': 'thmedia'},
        28: {'command': 'rfkit/bt1', 'emitter': 'broadlink', 'signal': 'rf'},
        29: {'channel': 2, 'command': 'rfkit/bt2', 'emitter': 'thmedia'},
        30: {'channel': 1, 'commands': {'50': {'protocol': 'NECx2',
             'device': 7, 'subdevice': 7, 'function': 15}},
             'emitter': 'thmedia'},
        3: {'command':
            {'protocol': 'NECx2', 'device': 7, 'subdevice': 7, 'function': 15},
            'emitter': 'broadlink'},
        5: {'command':
            {'protocol': 'NECx2', 'device': 7, 'subdevice': 7, 'function': 16},
            'emitter': 'broadlink', 'mac': 'aa_bb_cc_dd_ee_ff'}}

    yaml_file = """
2:
  emitter: broadlink
  command: tv/samsung/channelup
8:
  emitter: broadlink
  command: tv/samsung/channeldown

1:
  emitter: broadlink
  command: tv/samsung/volumeup
7:
  emitter: broadlink
  command: tv/samsung/volumedown
14:
  emitter: broadlink
  command: tv/samsung/mute

4:
  emitter: broadlink
  command: tv/samsung/up
10:
  emitter: broadlink
  command: tv/samsung/down
9:
  emitter: broadlink
  command: tv/samsung/left
11:
  emitter: broadlink
  command: tv/samsung/right

25:
  emitter: broadlink
  command: tv/samsung/power

26:
  emitter: thmedia
  command: tv/samsung/power

28:
  signal: rf
  emitter: broadlink
  command: rfkit/bt1
29:
  emitter: thmedia
  command: rfkit/bt2
  channel: 2
30:
  emitter: thmedia
  commands:
    50:
      protocol: NECx2
      device: 7
      subdevice: 7
      function: 15

3:
  emitter: broadlink
  command:
    protocol: NECx2
    device: 7
    subdevice: 7
    function: 15
5:
  emitter: broadlink
  mac: aa_bb_cc_dd_ee_ff
  command:
    prootocol: NECx2
    device: 7
    subdevice: 7
    function: 16"""

    @patch('RFIRLumino.RFIRLumino.command_to_mqtt')
    @patch('RFIRLumino.RFIRLuminoThmedia.activate')
    @patch('RFIRLumino.RFIRLuminoBroadlink.activate')
    def test_activate(self, mock_broadlink, mock_thmedia,
                      mock_command_to_mqtt):
        mock_mqtt_client = MagicMock()
        with patch('os.path.exists',
                   MagicMock(return_value=True)) as m:
            with patch('os.path.getctime',
                       MagicMock(return_value=True)) as n:
                with patch('builtins.open',
                           mock_open(read_data=self.yaml_file)) as o:
                    luminos = RFIRLuminoList(mock_mqtt_client,
                                             "foo/bar", self.yaml_file)
                    m.assert_called_once_with(self.yaml_file)
                    n.assert_called_once_with(self.yaml_file)
                    o.assert_called_once_with(self.yaml_file)

        for k in self.config.keys():
            luminos.activate(k, self.intensity)
        luminos.activate(123123, self.intensity)
        self.assertEqual(mock_broadlink.call_count, 13)
        self.assertEqual(mock_thmedia.call_count, 3)
        self.assertEqual(mock_command_to_mqtt.call_count, 16)

    def test_activate_invalid_emitter(self):
        self.assertRaises(Exception, RFIRLuminoList, None, None,
                          {3: {"emitter": "foo"}})

    @patch('RFIRLumino.RFIRLuminoThmedia.activate')
    @patch('RFIRLumino.RFIRLuminoBroadlink.activate')
    def test_activate_invalid_yaml(self, mock_broadlink, mock_thmedia):
        mock_logging = MagicMock()
        with patch('os.path.exists',
                   MagicMock(return_value=True)) as m:
            with patch('os.path.getctime',
                       MagicMock(return_value=True)) as n:
                with patch('builtins.open',
                           mock_open(read_data="test.yaml")) as p:
                    with patch('yaml.load',
                               MagicMock(side_effect=Exception)):
                        o = RFIRLuminoList(None, "foo/bar", "test.yaml",
                                           logging=mock_logging)
                        m.assert_called_once_with("test.yaml")
                        n.assert_called_once_with("test.yaml")
                        p.assert_called_once_with("test.yaml")

        for k in self.config.keys():
            o.activate(k, self.intensity)
        o.activate(123123, self.intensity)
        self.assertEqual(mock_broadlink.call_count, 0)
        self.assertEqual(mock_thmedia.call_count, 0)
